var t = require('./form/components.js');
var templates = require('./form/templates/bootstrap');
var i18n = require('./form/i18n/en');
t.Form.templates = templates;
t.Form.i18n = i18n;
module.exports = t.Form;
