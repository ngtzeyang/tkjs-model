var NI = require('tkjs-ni');
var t = require('./tcomb-validation');
t.String = t.Str;
t.Date = t.Dat;
t.Number = t.Num;
t.Int = t.Num;
t.Float = t.Num;
t.Array = t.Arr;
t.Object = t.Obj;
t.Error = t.Err;
t.optional = t.maybe;

//==========================================================================
// User name
//==========================================================================
// UserName example: admin, roger_charle

t.UserName_regex = /^[a-z0-9_]{2,25}$/;
t.UserName_errorMessage = 'can only contain lower case letters, digits and _ and must be between 2 and 25 characters long.'
t.UserName = t.subtype(t.Str, function (s) {
  return s.length > 0 && t.UserName_regex.test(s);
});

//==========================================================================
// UUID
//==========================================================================
// UUID example: 1cabcaaf-b361-47e7-9c7a-a17148ca78d7

t.UUID_regex = /^[0-9a-f]{8}-?[0-9a-f]{4}-?[1-5][0-9a-f]{3}-?[89ab][0-9a-f]{3}-?[0-9a-f]{12}$/i;
t.UUID_errorMessage = 'invalid format.'
t.UUID = t.subtype(t.Str, function (s) {
  return s.length > 0 && t.UUID_regex.test(s);
});

//==========================================================================
// Email
//==========================================================================

t.Email_regex = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
t.Email_errorMessage = 'invalid email address.'
t.Email = t.subtype(t.Str, function (s) {
  return s.length > 0 && t.Email_regex.test(s);
});

//==========================================================================
// Password
//==========================================================================
function computePasswordStrength(aPwd) {
  var strength = 0;
  // upper case +1
  strength += /[A-Z]+/.test(aPwd) ? 1 : 0;
  // lower case +1
  strength += /[a-z]+/.test(aPwd) ? 1 : 0;
  // digit +1
  strength += /[0-9]+/.test(aPwd) ? 1 : 0;
  // special +1
  strength += /[\W]+/.test(aPwd) ? 1 : 0;
  return strength;
}
t.computePasswordStrength = computePasswordStrength;

t.Password_errorMessage = 'password should be 6 characters long with at least one letter and one digit.'
t.Password = t.subtype(t.Str, function (s) {
  return s.length >= 6 && computePasswordStrength(s) >= 2;
});

//==========================================================================
// Utils
//==========================================================================
function assertValidate(aPayload,aModel) {
  if (typeof aModel === "string") {
    aModel = getModel(aModel);
  }
  // Undefined, null and an empty object are all considered null.  This allow
  // us to enforce empty payloads.
  if (!aPayload || NI.isEmpty(aPayload)) {
    aPayload = null;
  }
  var r = t.validate(aPayload,aModel);
  if (!r.isValid()) {
    var errMsg = NI.map(r.errors,function(e) { return e.message; }).join(", ");
    throw NI.Error("Invalid Payload: %s", errMsg);
  }
  return aPayload;
}
t.assertValidate = assertValidate;

var _wasValidate = t.validate;
function validate(aPayload, aModel) {
  if (typeof aModel === "string") {
    aModel = getModel(aModel);
  }
  return _wasValidate(aPayload, aModel);
}
t.validate = validate;

//==========================================================================
// Registry
//==========================================================================
var _models = {
  ":empty": t.Nil,
  ":any": t.union([t.Nil,t.Object])
}
var _modelsDefaultValue = {}

t.registerModel = function registerModel(aName,aStructDef,aDefaultValues) {
  NI.assert.isString(aName);
  NI.assert.isObject(aStructDef);
  if (aName in _models) {
    throw NI.Error("Model '%s' already registered.", aName);
  }
  if (!NI.stringContains(aName,":")) {
    throw NI.Error("Model name '%s' should be of the form 'module_name:model_name'.", aName);
  }
  var model = t.struct(aStructDef, aName);
  _models[aName] = model;
  if (aDefaultValues) {
    _modelsDefaultValue[aName] = model;
  }
  return model;
}

function hasModel(aName) {
  return !!_models[aName];
}
t.hasModel = hasModel;

function getModel(aName) {
  var model = _models[aName];
  if (!model) {
    throw NI.Error("Can't find Model '%s'.", aName);
  }
  return model;
}
t.getModel = getModel;

function getModelDefaultValues(aName) {
  var model = _models[aName];
  if (!model) {
    throw NI.Error("Can't find Model '%s'.", aName);
  }
  return _modelsDefaultValue[aName];
}
t.getModelDefaultValues = getModelDefaultValues;

module.exports = t;
